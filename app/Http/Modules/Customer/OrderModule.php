<?php

namespace App\Http\Modules\Customer;

use Auth;
use App\Models\Customer;
use App\Models\PriceRule;

use App\Http\Rules\Customer\ExistCheck;

use App\Http\Helpers\General;
use App\Http\Helpers\Checkout;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class OrderModule
{
    public function __construct()
    {
        
    }

    public static function checkout($request)
    {
        $validation = OrderModule::validation($request, '', 'CHECKOUT');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        $product_id = $request->input('product_id');
        $pricing_rule = PriceRule::where('status', config('constants.status.active'))->get();

        $checkout = new Checkout($pricing_rule);
        $scan = $checkout->scan($product_id);

        $data = (object)[
            'status' => true,
            'data' => $checkout->total()
        ];

        return response()->json($data);
    }

    private static function validation(Request $request, $id = '', $method = 'POST')
    {
        $data = $request->all();

        $rule= [];
        if ($method == 'CHECKOUT') {
            $rule = [
                'product_id' => ['required', new ExistCheck('product_code')],
            ];
        }

        $validator = Validator::make($data, $rule, config('error_code'));

        $errors = $validator->errors();

        if ($validator->fails()) {
            $data = (object)[
                'status' => false,
                'errors' => $errors
            ];
            return $data;
        }
        else {
            return (object)['status' => true];
        }
    } 
}
