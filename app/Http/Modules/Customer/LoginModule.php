<?php

namespace App\Http\Modules\Customer;

use Auth;
use App\Models\Customer;
use App\Http\Rules\Customer\LoginRule;

use App\Http\Helpers\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LoginModule
{
    public function __construct()
    {
        
    }

    public static function login(Request $request)
    {
        $validation = LoginModule::validation($request, '', 'LOGIN');
        if (!$validation->status) {
            return response()->json($validation, 422);
        }

        $email = $request->input('email');

        $customer = Customer::
            where('email', $email)
            ->first();

        $token = $customer->createToken($customer->id, ['only-customer']);

        $data = (object)[
            'status' => true,
            'access_token' => $token->accessToken,
            'data' => $customer
        ];

        return response()->json($data);
    }

    private static function validation(Request $request, $id = '', $method = 'POST')
    {
        $data = $request->all();

        $rule= [];
        if ($method == 'LOGIN') {
            $rule = [
                'email' => ['required', new LoginRule('manual', $data['password'] ?? '')],
                'password' => ['required']
            ];
        }

        $validator = Validator::make($data, $rule, config('error_code'));

        $errors = $validator->errors();

        if ($validator->fails()) {
            $data = (object)[
                'status' => false,
                'errors' => $errors
            ];
            return $data;
        }
        else {
            return (object)['status' => true];
        }
    } 
}
