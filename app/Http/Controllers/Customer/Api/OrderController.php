<?php

namespace App\Http\Controllers\Customer\Api;

use Auth;
use Illuminate\Http\Request;

use App\Http\Modules\Customer\OrderModule;

use App\Http\Controllers\Customer\BaseController;

class OrderController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function checkout(Request $request)
    {
    	$order = OrderModule::checkout($request);
    	return $order;
    }
}
