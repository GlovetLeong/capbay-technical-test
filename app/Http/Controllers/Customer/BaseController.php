<?php

namespace App\Http\Controllers\Customer;
use Auth;
use App\Http\Controllers\Controller;

class BaseController extends Controller{
	
	public function __construct() {
		\Config::set('auth.guards.api.provider', 'customers');
		$this->middleware(['auth:customer-api', 'scope:only-customer']);
	}
}
