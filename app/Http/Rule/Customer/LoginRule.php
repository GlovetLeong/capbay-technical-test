<?php

namespace App\Http\Rules\Customer;

use Auth;
use App\Models\Customer;

use Illuminate\Contracts\Validation\Rule;
use OTPHP\TOTP;

class LoginRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    protected $type;
    protected $password;

    public function __construct(
        $type = '', 
        $password = ''
    )
    {
        $this->type = $type;
        $this->password = $password;
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->type == 'manual') {
            $email = $value;
            $password = $this->password;

            if (Auth::guard('customer')->attempt(['email' => $email, 'password' => $password])) {
                $data = Customer::
                        where('email', $email)
                        ->whereIn('status', [1, 9])
                        ->count();
                    if (!$data) {
                        return false;
                    }
                return true;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return config('error_code.login_error');
    }
}
