<?php
namespace App\Http\Helpers;

use App\Models\Product;

class Checkout
{
	private $pricing_rule = [];
	private $final_scan_result = [];

	function __construct($pricing_rule = [])
	{
		$this->pricing_rule = $pricing_rule;
	}

	private function classItem($item)
	{
		$class_item = [];
		foreach ($item as $key_class => $row_class) {
			if (empty($class_item[$row_class])) {
				$class_item[$row_class] = 1;
			}
			else {
				$class_item[$row_class] += 1;
			}
		}
		return $class_item;
	}

	private function scanResult($class_item)
	{
		$scan_result = [];
		foreach ($class_item as $key_scan => $row_scan) {
			$product = Product::where('code', $key_scan)->first();

			$scan_result[$key_scan] = (object)[
				'code' => $key_scan,
				'name' => $product->name,
				'quantity' => $row_scan,
				'price' => $product->price,
				'total' => $product->price * $row_scan
			];
		}

		return $scan_result;
	}

	private function quaterCalculate($quantity = 0, $rule_unit = 0, $free_unit = 0, $history = [], $type = '')
	{

		$remain = $remain_unit = $quantity - $rule_unit;
		if ($type == 'same') {
			$remain = $remain_free = $remain_unit - $free_unit;
		}

		$pay = !empty($history) ? $history->pay + ($remain_unit > 0 ? $rule_unit : $quantity) : $rule_unit;
		$free = !empty($history) ? $history->free + ($remain_unit >= 0 ? $free_unit : 0) : $free_unit;
		$history = (object)[
			'pay' => $pay,
			'free' => $free
		];
		if($remain > 0) {
			return $this->quaterCalculate($remain, $rule_unit, $free_unit, $history, $type);
		}
		return $history;
	}

	private function getScanTotal($scan_result = [])
	{
		$total = 0;
		foreach ($scan_result as $key => $row) {
			$total += $row->total;
		}
		return $total;
	}

	private function pricingRuleCalculate($scan_result)
	{
		$final_scan_result = [];
		$pricing_rule = $this->pricing_rule;
		$FREE = [];
			foreach ($pricing_rule as $key_rule => $row_rule) {
				$RULE_code = $row_rule->product()->first()->code;
				$RULE_TYPE = $row_rule->type;

				if ($RULE_TYPE == config('constants.price_rule_type.free_same_item')) {
					$FREE_PRODUCT = $row_rule->free_product()->first();
					$RULE_UNIT = $row_rule->unit_trigger;
					$RULE_FREE_UNIT = $row_rule->free_unit;
					$SCAN_ITEM = !empty($scan_result[$RULE_code]) ? $scan_result[$RULE_code] : [];
					$SCAN_ITEM_FREE = !empty($scan_result[$FREE_PRODUCT->code]) ? $scan_result[$FREE_PRODUCT->code] : [];
					if (!empty($SCAN_ITEM)) {
						if ($SCAN_ITEM->quantity >= $RULE_UNIT && $SCAN_ITEM->quantity > 1) {
							$QUATER_RESULT = $this->quaterCalculate($SCAN_ITEM->quantity, $RULE_UNIT, $RULE_FREE_UNIT, [], 'same');
							if($RULE_UNIT < $SCAN_ITEM->quantity) {
								$SCAN_ITEM->total = $QUATER_RESULT->pay * $SCAN_ITEM->price;
							}
							$FREE[] = (object)[
								'type' => 'free_same_item',
								'code' => $FREE_PRODUCT->code,
								'name' => $FREE_PRODUCT->name,
								'quantity' => $QUATER_RESULT->free, 
								'price' => $FREE_PRODUCT->price,
								'total' => $QUATER_RESULT->free * $FREE_PRODUCT->price
							];
						}
					}
				}

				if ($RULE_TYPE == config('constants.price_rule_type.discount_same_item_price')) {
					$RULE_DISCOUNT_ACCOUNT =  $row_rule->discount_price;
					$RULE_UNIT = $row_rule->unit_trigger;
					$ORGINAL_PRODUCT = $row_rule->product()->first();
					$SCAN_ITEM = !empty($scan_result[$RULE_code]) ? $scan_result[$RULE_code] : [];
					if (!empty($SCAN_ITEM)) {
						if ($SCAN_ITEM->quantity >= $RULE_UNIT) {
							$SCAN_ITEM->price = $RULE_DISCOUNT_ACCOUNT;
							$SCAN_ITEM->total = $SCAN_ITEM->quantity * $RULE_DISCOUNT_ACCOUNT;
							$FREE[] = (object)[
								'type' => 'discount_same_item_price',
								'code' => $SCAN_ITEM->code,
								'name' => $FREE_PRODUCT->name,
								'quantity' => $SCAN_ITEM->quantity, 
								'original_price' => $ORGINAL_PRODUCT->price,
								'price' => $SCAN_ITEM->price,
								'total' => $SCAN_ITEM->quantity * $RULE_DISCOUNT_ACCOUNT,
								'total_discount' => number_format(($SCAN_ITEM->quantity  * $ORGINAL_PRODUCT->price) - ($SCAN_ITEM->quantity * $RULE_DISCOUNT_ACCOUNT), 2)
							];
						}
					}
				}

				$total = $this->getScanTotal($scan_result);

				$final_scan_result = (object)[
					'items' => $scan_result,
					'free' => $FREE,
					'total' => 'RM' . $total
				];
			}

		$this->final_scan_result[] = $final_scan_result;
	}

	public function scan($item = [])
	{
		$class_item = $this->classItem($item);
		$scan_result = $this->scanResult($class_item);
		$final_scan_result = $this->pricingRuleCalculate($scan_result);
	}

	public function total()
	{
		return $this->final_scan_result;
	}
}