<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceRule extends Model
{
    protected $table = 'price_rules';

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function free_product()
    {
        return $this->belongsTo('App\Models\Product', 'free_product_id');
    }
}
