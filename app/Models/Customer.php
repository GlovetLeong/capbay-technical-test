<?php

namespace App\Models;

use App\Http\Helpers\ImageHandel;

use Laravel\Passport\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard_name = 'customer';
    
    protected $table = 'customers';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function getImgAttribute($value)
    {
        $file_info = [];
        $file_info = ImageHandel::getImgFullPath($value, 'person');
        return $file_info;
    }

    public function getUpdatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }

    public function getCreatedAtAttribute($date) 
    {
        return $date ? Carbon::parse($date)->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s') : '';
    }
}
