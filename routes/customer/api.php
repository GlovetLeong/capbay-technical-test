<?php

Route::get('unauthorized', ['as' => 'unauthorized', function() {
    return response('Unauthorized Access', 401);
}]);

Route::group(['namespace' => 'Customer', 'prefix' => 'customer-backsys/api'], function () {
	Route::group(['namespace' => 'Api'], function () {
    	Route::post('login', ['as' => 'api.customer.login', 'uses' => 'LoginController@login']);
		// Customer
		Route::prefix('customer')->group(function () {
			Route::get('self', 
	    		[
					'as' => 'api.customer.self', 
					'uses' => 'UserController@self', 
	    		]
	    	);
		});

		Route::prefix('order')->group(function () {
			Route::post('checkout', 
	    		[
					'as' => 'api.customer.order.checkout', 
					'uses' => 'OrderController@checkout', 
	    		]
	    	);
		});
	});
});