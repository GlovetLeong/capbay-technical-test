<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PriceRulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_rules')->insert([
            [
                'id' => 1,
                'product_id' => 1,
                'free_product_id' => 1,
                'description' => 'Buy One Kopi Free One Kopi',
                'unit_trigger' => 1,
                'free_unit' => 1,
                'discount_price' => null,
                'type' => config('constants.price_rule_type.free_same_item'),
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'product_id' => 2,
                'free_product_id' => 2,
                'description' => 'Discount price for Teh Tarik if more than 2',
                'unit_trigger' => 2,
                'free_unit' => 20,
                'discount_price' => 1.2,
                'type' => config('constants.price_rule_type.discount_same_item_price'),
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'product_id' => 3,
                'free_product_id' => 3,
                'description' => 'Buy One Roti Kosong Free One Roti Kosong',
                'unit_trigger' => 1,
                'free_unit' => 1,
                'discount_price' => null,
                'type' => config('constants.price_rule_type.free_same_item'),
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}