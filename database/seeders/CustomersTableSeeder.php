<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'id' => 1,
                'first_name' => 'Leong',
                'last_name' => 'Chee Siong',
                'email' => 'cheesiong@capbay.com',
                'contact_number' => '0147206536',
                'password' => bcrypt('pass123'),
                'dob' => '1992-01-01',
                'gender' => config('constants.gender.male'),
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'first_name' => 'Leong',
                'last_name' => 'Glovetloxad',
                'email' => 'glovet@capbay.com',
                'contact_number' => '0147206537',
                'password' => bcrypt('pass123'),
                'dob' => '1992-01-01',
                'gender' => config('constants.gender.male'),
                'status' => config('constants.status.active'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}