<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_rules', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->bigInteger('free_product_id')->unsigned()->nullable();
            $table->foreign('free_product_id')->references('id')->on('products')->onDelete('cascade');
            $table->string('description');
            $table->integer('unit_trigger')->nullable()->default(1);
            $table->integer('free_unit')->nullable()->default(1);
            $table->float('discount_price', 20, 4)->nullable();
            $table->integer('type')->nullable()->default(1);
            $table->integer('status')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_rules');
    }
}
