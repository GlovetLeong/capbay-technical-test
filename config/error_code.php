<?php
	return [
		'exist_error' => 'EXIST_ERROR',
		'under_error' => 'UNDER_ERROR',
		'campaign_expired' => 'CAMPAIGN_EXPIRED',
		'redeem_expired' => 'REDEEM_EXPIRED',
		'campaign_already_enter' => 'CAMPAIGN_ALREADY_ENTER',
		'campaign_transaction_not_more_than_3' => 'CAMPAIGN_TRANSACTION_NOT_MORE_THAN_3',
		'campaign_transaction_spent_not_more_than_100' => 'CAMPAIGN_TRANSACTION_SPENT_NOT_MORE_THAT_100',
		'unique_error' => 'UNIQUE_ERRO',
		'login_error' => 'LOGIN_ERROR',
		'required' => 'REQUIRED',
		'required_if' => 'REQUIRED',
		'email' => 'EMAIL_ERROR',
		'unique' => 'UNIQUE_ERRO',
		'date' => 'DATE_INVALID',
		'confirmed' => 'CONFIRM_ERROR',
		'status_error' => 'STATUS_ERROR',
		'current_password_not_match' => 'CURRENT_PASSWORD_NOT_MATCH',
		'access_error' => 'ACCESS_ERROR',
		'min' => [
	        'string' => 'MIN_ERROR_:min',
	    ],
	    'numeric' => 'NUMERIC_ERROR',
	    'gt' => [
	    	'numeric' => 'GREATER_ERROR_:value'
	    ],
	    'lt' => [
	    	'numeric' => 'LESS_ERROR_:value'
	    ],
	];