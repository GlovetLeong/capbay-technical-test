<?php

return [
    'status' => [
        'active' => 1,
        'deactive' => 2,
        'delete' => 3,
        'clear' => 4,
        'pending' => 9,
        'reject_login' => 14,
    ],
    'gender' => [
        'male' => 1,
        'female' => 1,
    ],
    'price_rule_type' => [
        'free_same_item' => 1,
        'discount_same_item_price' => 2,
    ]
];
