<?php

namespace Tests\Feature;

use App\Models\Customer;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiCustomerTest extends TestCase
{
    public function user_auth_token() 
    {
        $customer = Customer::find(1);
        $token = $customer->createToken($customer->id, ['only-customer']);
        return $token->accessToken;
    }

    public function test_login()
    {
        $response = $this->post('customer-backsys/api/login', [
            'email' => env('TEST_USER_EMAIL'),
            'password' => env('TEST_USER_PASSWORD')
        ]);
        $response->assertStatus(200);
    }

    public function test_customer_self()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->get('customer-backsys/api/customer/self');

        $response->assertStatus(200);
    }
}
