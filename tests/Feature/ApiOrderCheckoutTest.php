<?php

namespace Tests\Feature;

use App\Models\Customer;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiOrderCheckoutTest extends TestCase
{
    public function user_auth_token() 
    {
        $customer = Customer::find(1);
        $token = $customer->createToken($customer->id, ['only-customer']);
        return $token->accessToken;
    }

    public function test_checkout_1()
    {
        $product_id = ['B001','F001','B002','B001','F001'];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->post('customer-backsys/api/order/checkout', [
             'product_id' => $product_id
        ]);

        fwrite(STDERR, print_r(implode(',', $product_id) . ' = ' . $response->original->data[0]->total . PHP_EOL, TRUE));

        $response->assertStatus(200);
    }

    public function test_checkout_2()
    {
        $product_id = ['B002','B002','F001'];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->post('customer-backsys/api/order/checkout', [
             'product_id' => $product_id
        ]);

        fwrite(STDERR, print_r(implode(',', $product_id) . ' = ' . $response->original->data[0]->total . PHP_EOL, TRUE));

        $response->assertStatus(200);
    }

    public function test_checkout_3()
    {
        $product_id = ['B001','B001','B002'];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user_auth_token()
        ])->post('customer-backsys/api/order/checkout', [
             'product_id' => $product_id
        ]);

        fwrite(STDERR, print_r(implode(',', $product_id) . ' = ' . $response->original->data[0]->total . PHP_EOL, TRUE));

        $response->assertStatus(200);
    }
}
