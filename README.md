# Capbay Technical Test

## Project
- php 7.3 >
- Apache
- Mysql
- laravel 8

## Installation
- git clone https://gitlab.com/GlovetLeong/capbay-technical-test.git
- cp .env.example .env
- edit .env db var to connect your local db
- composer install
- php artisan migrate:refresh --seed
- php artisan passport:install


## Unit Test
- vendor\bin\phpunit
- execute test result
- B001,F001,B002,B001,F001 = RM6.9
- B002,B002,F001 = RM3.5
- B001,B001,B002 = RM4.5

## Api Doc
- capBay.postman_collection.json

## Note
- to call order/checkout endpoint user need provide bearer token (refer api doc)
- Tests\Feature\ApiOrderCheckoutTest file to edit the test data


